const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 

function stringToNumber (string) {
    if(typeof(string)=='string') {
      let result = "";
      let StrToArray=string.split("");
      StrToArray.filter((char)=>{
        if(char!='$' && char!='M'){
            result+=char;
        }
      });
      if(isNaN(result)){
        return 0
      }
      //console.log(result);
      return parseFloat(result);
    }
    return 0;
}




function earning(data,amountInMillion){
    let dataInArray = Object.keys(data);
    let ansObj = dataInArray.reduce((accumulator,movieName)=>{
        if(stringToNumber(data[movieName].totalEarnings)>=amountInMillion){
            accumulator[movieName]=data[movieName];
        }
        return accumulator
    },[]);
    return ansObj;
}   
// oscarNominationAndBigEarning(favouritesMovies,3,500);



function oscarNominationAndBigEarning(data,numOfOscarNom,earningInMillions){
    let mostEarningMovies = earning(data,earningInMillions);

    let movieName = Object.keys(mostEarningMovies);
    console.log(movieName);
    let ansObj = movieName.reduce((accumulator,movie)=>{
        if(mostEarningMovies[movie].oscarNominations>numOfOscarNom){
            accumulator[movie]= mostEarningMovies[movie];
        }
        return accumulator;
    },[]);
    return ansObj;
}



function actorsMovie(data,actorName){
    let movieNames = Object.keys(data);

    let ansObj = movieNames.reduce((accumulator, movieName)=>{
        let actors = data[movieName].actors;
    
        let ifActorPresent = actors.reduce((status,actor)=>{
            if(actor==actorName){
                status = true;
            }
            return status;
        },false);
        if(ifActorPresent){
            accumulator[movieName]=data[movieName];
        }
        return accumulator;
    },[]);
    return ansObj;
}

// actorsMovie(favouritesMovies,'Leonardo Dicaprio');



function sort(data){
    let values = Object.values(data);
    let ansObj = values.sort((match1,match2) =>{
        if(match1.imdbRating > match2.imdbRating){
            return 1;
        }else if(match1.imdbRating < match2.imdbRating) {
            return -1;
        }
        if(stringToNumber(match1.totalEarnings)>stringToNumber(match2.totalEarnings)){
            return 1;
        }else{
            return -1;
        }
    });
    return ansObj;
}
// sort(favouritesMovies);






const groupingGeners = ["drama", "sci-fi","adventure","thriller","crime"];
function groupByGeners(data){
    let key = Object.keys(data);
    let ansObj=groupingGeners.reduce((accumulator,movieGener)=>{
        accumulator[movieGener]=key.reduce((acc,movie)=>{
            let str = data[movie].genre.toString();
            if(str.includes(movieGener)){
                acc[movie]=data[movie];
            }
            return acc;
        },{});
        return accumulator;
    },{});
    console.log(ansObj);

}
groupByGeners(favouritesMovies);