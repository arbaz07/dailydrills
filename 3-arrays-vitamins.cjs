const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 
//1. Get all items that are available 
function availableItems(itemData){
    let availables = itemData.filter((item)=>{
        return item.available;
    });
    // console.log(availables);
    return availables;
}
// availableItems(items);
/*
Answer =>
[
    { name: 'Orange', available: true, contains: 'Vitamin C' },
    { name: 'Mango', available: true, contains: 'Vitamin K, Vitamin C' },
    { name: 'Pineapple', available: true, contains: 'Vitamin A' }
]
*/









//2. Get all items containing only Vitamin C.
function containsVitaminC(itemsData){
    let itemContainVitaminC = itemsData.filter((item)=>{
        return item.contains=='Vitamin C';
    });

    // console.log(itemContainVitaminC);
    return itemContainVitaminC
}
// containsVitaminC(items);
/*
Answer =>
[ { name: 'Orange', available: true, contains: 'Vitamin C' } ]
*/







// 3. Get all items containing Vitamin A.
function containsVitaminA(itemsData){
    let itemContainVitaminA = itemsData.filter((item)=>{
        let vitaminsInItem = item.contains;
        return vitaminsInItem.includes('Vitamin A');
    });
    // console.log(itemContainVitaminA);
    return itemContainVitaminA;
}
// containsVitaminA(items);
/*
Answer =>
[
  { name: 'Pineapple', available: true, contains: 'Vitamin A' },
  {
    name: 'Raspberry',
    available: false,
    contains: 'Vitamin B, Vitamin A'
  }
]

*/









/*4. Group items based on the Vitamins that they contain in the following format:
{
    "Vitamin C": ["Orange", "Mango"],
    "Vitamin K": ["Mango"],
}
and so on for all items and all Vitamins.
*/

function groupByVitamins(itemsData){
    let listOfVitamins = new Set();

    //creating an array of only unique vitamins
    itemsData.filter((item)=>{
        let arr=item.contains.split(", ");
        if(arr.length==1){
            listOfVitamins.add(arr[0]);
        }else{
            arr.filter((i)=>{
                listOfVitamins.add(i);
            });
        }
    });
    //call to unction which will return an abswer object
    let answerObj = checkIfIncludes(listOfVitamins,itemsData);
    return answerObj;
}

function checkIfIncludes(vitaminList, itemData){

    let ansObj={};
   
    //converting Set into array
    let listOfVitamins  = Array.from(vitaminList);
    
    listOfVitamins.filter((vitamin)=>{

        let arr=[]
        itemData.filter((subItem)=>{
            //console.log(subItem.contains);
            let str=subItem.contains;
            if(str.includes(vitamin)){
                arr.push(subItem.name);
                //console.log(subItem.name);
            }
        });

        //adding key as vitamin name and value as name of items which contain that vitamin
        ansObj[vitamin]=arr;
      
    });
    return ansObj;
}
// groupByVitamins(items);
/*
Answer =>
{
  'Vitamin C': [ 'Orange', 'Mango' ],
  'Vitamin K': [ 'Mango' ],
  'Vitamin A': [ 'Pineapple', 'Raspberry' ],
  'Vitamin B': [ 'Raspberry' ],
  'Vitamin D': [ 'Grapes' ]
}
*/









// 5. Sort items based on number of Vitamins they contain.
function sortBaseOnVitaminCount(itemData){
    // let updatedItemData={};
    itemData.filter((item)=>{
        let arr=item.contains.split(", ");
        item.NumberOfVitamins=arr.length;
    });
    //console.log(itemData);
    itemData.sort(function compareFn(item1, item2) {
        if (item1.NumberOfVitamins < item2.NumberOfVitamins) {
            return -1;
        }
        if (item1.NumberOfVitamins > item2.NumberOfVitamins) {
            return 1;
        }
        return 0;
    });
    // console.log(itemData);
    return itemData;
}
// sortBaseOnVitaminCount(items);
/*
Answer =>
[
  {
    name: 'Orange',
    available: true,
    contains: 'Vitamin C',
    NumberOfVitamins: 1
  },
  {
    name: 'Pineapple',
    available: true,
    contains: 'Vitamin A',
    NumberOfVitamins: 1
  },
  {
    name: 'Grapes',
    contains: 'Vitamin D',
    available: false,
    NumberOfVitamins: 1
  },
  {
    name: 'Mango',
    available: true,
    contains: 'Vitamin K, Vitamin C',
    NumberOfVitamins: 2
  },
  {
    name: 'Raspberry',
    available: false,
    contains: 'Vitamin B, Vitamin A',
    NumberOfVitamins: 2
  }
]
*/