
const path=require("path");
const fs=require("fs");

const filePath1 = "./file_1.cjs";
const filePath2 = "./file_2.cjs";






/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.
*/



/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/
function problem3(user,val) {
    // use promises and fs to save activity in some file
    //A
   
    login(user,val)
    .then((response)=>{
        logData(user,"Login Success");
        console.log(response);
        getData()
        .then((response)=>{
            logData(user,"getData Success")
            console.log(response);
        })
        .catch(err=>{
            logData(user,"getData Failure");
            console.log(err.message);
        })
    })
    .catch(err=>{
        logData(user,"Login Failure");
        console.log(err.message);
    });
}
problem3("Arbaz",3);




function logData(user,activity){
    let history = activity+" By user : "+user+" at : "+new Date();
    fs.appendFile(path.join(__dirname,"./History.txt"),history+"\n",(err)=>{
        if(err){
            console.error(err);
        }else{

        }
    });
}


function login(user, val) {
    if(val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1, 
            name: "Test",
        },
        {
            id: 2, 
            name: "Test 2",
        }
    ]);
}

/*
Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)
*/
function problem1(){

    new Promise((resolve,reject)=>{
        fs.writeFile(path.join(__dirname,filePath1),"1-Hi this is Arbaz Pathan.","utf-8",(err)=>{
            if(err){
                reject(err);
                console.error("error occured in createion of file 1"+err);
            }else{
                console.log(filePath1+" got created.");
                resolve();
            }
        });
    });
    new Promise((resolve,reject)=>{
        fs.writeFile(path.join(__dirname,filePath2),"2-Hi this is Arbaz Pathan.","utf-8",(err)=>{
            if(err){
                reject("error occured in createion of file 2"+err);
            }else{
                console.log(filePath2+" got created.");
                resolve();
            }
        });
    });

    setTimeout(()=>{
        fs.unlink(path.join(__dirname,filePath1),(err)=>{
            if(err){
                console.error("error while deleting");
            }else{
                console.log(filePath1+" got deleted successfully");
                
                fs.unlink(path.join(__dirname,filePath2),(err)=>{
                    if(err){
                        console.error(err);
                    }else{
                        console.log(filePath2+" got deleted successfully")
                    }
                });
            }
        });  
    },2*1000);
}

// problem1();






/*
Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/

function problem2(){
    const lipsumData =`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis elit nisl. Aliquam congue ut orci quis gravida. Etiam eleifend ultrices turpis, ut gravida ligula accumsan vel. Cras sagittis ex ac rutrum congue. Curabitur tristique nibh posuere nisi varius, non ullamcorper sapien venenatis. Maecenas non suscipit ex. Etiam commodo magna ullamcorper ex dignissim, sit amet lacinia dolor molestie. Morbi vitae scelerisque magna. Phasellus pulvinar, mauris id ornare consequat, elit sem fermentum nisl, eget varius sapien risus at felis.`; 

    new Promise((resolve,reject)=>{
        fs.writeFile(path.join(__dirname,"./lipsum.txt"),lipsumData,(err)=>{
            if(err){
                console.error(err);
                reject(err);
            }else{
                console.log("Created lipsum txt");
                resolve();
            }
        })
    })
    .then(()=>{
        return new Promise((resolve,reject)=>{
            fs.readFile(path.join(__dirname,"./lipsum.txt"),"utf-8",(err,data)=>{
                if(err){
                    reject(err);
                }else{
                    console.log("Read lipsum.txt");
                    resolve(data);
                }
            })
        })
    })
    .then((data)=>{
        return new Promise((resolve,reject)=>{
            fs.writeFile(path.join(__dirname,"./newLipsum.txt"),data,(err)=>{
                if(err){
                    reject(err);
                }else{
                    console.log("create newLipsum.txt and write data on to it");
                    resolve();
                }
            })
        })
    })
    .then(()=>{
        return new Promise((resolve,reject)=>{
            fs.unlink(path.join(__dirname,"./lipsum.txt"),(err)=>{
                if(err){
                    reject(err);
                }else{
                    console.log("deleted original lipsum.txt");
                    resolve();
                }
            });
        });
    })
    .catch((err)=>{
        console.error(err);
    })
}

// problem2();