
/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended

*/






function problem1(){
    fetch('https://jsonplaceholder.typicode.com/users')
    .then((usersResponse)=>{
        return usersResponse.json()        
    })
    .then((userData)=>{
        console.log(userData);

        console.log("Problem 1 Exicution has been completed!\n\n")
        //calling next function
        problem2();
    })
    .catch(err=>{
        console.error(err);
    });
}






function problem2(){
       //fetching all the todos data
    fetch('https://jsonplaceholder.typicode.com/todos')
    .then((todosResponse)=>{
        return todosResponse.json();
    })
    .then((todosData)=>{
        console.log(todosData);

        console.log("Problem 2 Exicution has been completed!\n\n")
        //calling next function
        problem3();
    })
    .catch((err)=>{
        console.error(err)
    })
}








function problem3(){
    fetch('https://jsonplaceholder.typicode.com/users')
    .then((usersResponse)=>{
        return usersResponse.json()        
    })
    .then((userData)=>{
        console.log(userData);

        //fetching all the todos data
        return fetch('https://jsonplaceholder.typicode.com/todos')
    })
    .then((todosResponse)=>{
        return todosResponse.json();
    })
    .then((todosData)=>{
        console.log(todosData);

        console.log("Problem 3 Exicution has been completed!\n\n")
        //calling next function
        problem4();
    })
    .catch((err)=>{
        console.error(err)
    })
    
}





// 4. Use the promise chain and fetch the users first and then all the details for each user.
function problem4(){
    fetch('https://jsonplaceholder.typicode.com/users')
    .then((usersResponse)=>{
        return usersResponse.json()        
    })
    .then((userData)=>{
        let promisesArr=[];
        // console.log(userData);
        Object.entries(userData).forEach((element)=>{
            let id = element[1].id;
            let promise = fetchForId(id);
            promisesArr.push(promise);
        });
        return Promise.all(promisesArr);
    })
    .then(()=>{
        console.log("Problem 4 Exicution has been completed!\n\n");
        problem5();
    })
    .catch(err=>{
        console.error(err);
    }); 

}



//5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo
function problem5(){
    fetch('https://jsonplaceholder.typicode.com/todos?id=1')
    .then((response)=>{
        return response.json();
    })
    .then((data)=>{
        let uId = data[0].userId;
        return fetch(`https://jsonplaceholder.typicode.com/users?id=${uId}`)
    })
    .then(response=>response.json())
    .then((data)=>{
        console.log(data);

        console.log("Problem 5 Exicution has been completed!\n\n");

    })
    .catch(err=>{
        console.error(err);
    })
}
problem1()





















function fetchForId(id){
    
    return fetch(`https://jsonplaceholder.typicode.com/todos?id=${id}`)
    .then(res=>res.json())
    .then(data=>console.log(data))
    .catch(err=>{
        console.error("error while fetching with id = "+id +"  :- "+err);
    });
}