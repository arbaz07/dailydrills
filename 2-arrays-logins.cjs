const data = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]

/* 

    1. Find all people who are Agender
    2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
    3. Find the sum of all the second components of the ip addresses.
    3. Find the sum of all the fourth components of the ip addresses.
    4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
    5. Filter out all the .org emails
    6. Calculate how many .org, .au, .com emails are there
    7. Sort the data in descending order of first name

    NOTE: Do not change the name of this file

*/

//1. Find all people who are Agender
function agenderPeople(data){
    let agender=[];
    data.map((people)=>{
        if(people.gender=="Agender"){
            agender.push(people);
        }
    });
    return agender;
    //console.log(agender);
}
//agenderPeople(data);


//2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
function spitIpAdd(data){
    let ipList=[];
    data.map((people)=>{
        //console.log(people.ip_address);
        let ip=people.ip_address.split(".").map(Number);
        ipList.push(ip);
        
    });
    return ipList
    //console.log(ipList);
}
//spitIpAdd(data);


//3. Find the sum of all the second components of the ip addresses.
function sumOfSecondComponentOfIp(data){
    let ArrayOfAllIpArray = spitIpAdd(data);
    const initialValue = 0;
    const ipSum = ArrayOfAllIpArray.reduce(
        (accumulator, currentValue) => accumulator + currentValue[1],
        initialValue
    );
    return ipSum;
    //console.log(ipSum)
}
//sumOfSecondComponentOfIp(data);


// 4. Find the sum of all the fourth components of the ip addresses.
function sumOffourthComponentOfIp(data){
    let ArrayOfAllIpArray = spitIpAdd(data);
    const initialValue = 0;
    const ipSum = ArrayOfAllIpArray.reduce(
        (accumulator, currentValue) => accumulator + currentValue[3],
        initialValue
    );
    return ipSum;
    //console.log(ipSum)
}
//sumOffourthComponentOfIp(data);


//5. Compute the full name of each person and store it in a new key (full_name or something) for each person.
function addfullNameAsKey(data){
    
    data.map((people)=>{
        let full_Name=people.first_name+" "+people.last_name;
        people['full_name']=full_Name;
        // console.log(full_Name);
        // console.log(people);
        
    });
    //returning data with added full_name key
    return data;

}
//addfullNameAsKey(data);


//5. Filter out all the .org emails
function emailsWithExtention(data,extention){
    let peopleWithEmail = data.filter((people)=>{
        let n= extention.length;
        let email=people.email;
        email=email.slice(email.length-n,email.length);
        return((email==extention) ? true : false);
    });
    //return peopleWithorgEmail
    return peopleWithEmail
    //console.log(peopleWithEmail);
}
//emailsWithExtention(data,".org");



//6. Calculate how many .org, .au, .com emails are there
function numberEmailWithExtension(data){
    let extensions = [".org",".au",".com"];
    let result={};
    let num=0;
    extensions.map((extension)=>{
        num=emailsWithExtention(data,extension).length;
        result[extension]=num;
  
        
    });
    
    console.log(result);
    return result;
}
numberEmailWithExtension(data);



//7. Sort the data in descending order of first name
function sortData(data){
    
    function compare( data1, data2 ) {
        if ( data1.first_name < data2.first_name ){
            return 1;
        }
        if ( data1.first_name > data2.first_name ){
            return -1;
        }
        return 0;
    }
    data.sort( compare );
    
    return data;
    //console.log(data);
}
//sortData(data);