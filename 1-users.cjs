const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 

function findInterest(data, key){
    const entries = Object.entries(data);
    let likings = [];
    let answerObj = entries.filter((acc,index)=>{
        likings = likings.concat(data[entries[index][0]].interests);
        if (likings[index].includes(key)){
            acc[entries[index][0]]=data[entries[index]];
            return acc;
        }
    },{});
    return answerObj
}
// findInterest(users,'Video Games');



function usersFromCountry(data, key){
    const entries = Object.entries(data);
    let answerObj = entries.filter((acc, index)=>{
        if(data[entries[index][0]].nationality==key){
            acc[entries[index][0]]=data[entries[index]];
            return acc;
        }   
    },{});
    return answerObj;
}
// usersFromCountry(users,"Germany");



function usersWithMasters(data, key){
    const entries = Object.entries(data);
    let answerObj = entries.filter((acc, index)=>{
        if(data[entries[index][0]].qualification==key){
            acc[entries[index][0]]=data[entries[index]];
            return acc;
        }   
    },{});
    return answerObj;
}
// usersWithMasters(users,"Masters")



function sortBasedOnEntity(data, sortKey){



    let key = Object.keys(data);
    let sorted = Object.entries(data);

    let counter = 0;
    sorted.sort(function compareFn(a, b) {
        console.log(a);
        let d=key[counter];
        console.log(a[d])
        if (data[b].age>data[a].age) {
          return -1;
        }
        if (data[b].age<data[a].age) {
          return 1;
        }
    
        return 0;
    });

    //return sorted;

}   
sortBasedOnEntity(users,"as")