
const array = [{"id":1,"first_name":"Gregg","last_name":"Lacey","job":"Web Developer III","salary":"$3.62","location":"Malta"},
{"id":2,"first_name":"Hernando","last_name":"Scargill","job":"Sales Associate","salary":"$4.16","location":"France"},
{"id":3,"first_name":"Loren","last_name":"Doull","job":"Physical Therapy Assistant","salary":"$4.34","location":"U.S. Virgin Islands"},
{"id":4,"first_name":"Sherwood","last_name":"Adriano","job":"Web Developer II","salary":"$6.46","location":"Brazil"},
{"id":5,"first_name":"Addison","last_name":"Dofty","job":"Structural Engineer","salary":"$6.40","location":"Costa Rica"},
{"id":6,"first_name":"Jess","last_name":"McUre","job":"Software Test Engineer I","salary":"$4.06","location":"Colombia"},
{"id":7,"first_name":"Nevil","last_name":"Matussevich","job":"Safety Technician I","salary":"$3.83","location":"Poland"},
{"id":8,"first_name":"Ivonne","last_name":"Lamping","job":"Analog Circuit Design manager","salary":"$3.07","location":"China"},
{"id":9,"first_name":"Sigvard","last_name":"Linggood","job":"Dental Hygienist","salary":"$5.34","location":"Russia"},
{"id":10,"first_name":"Edna","last_name":"Mundwell","job":"Biostatistician III","salary":"$9.28","location":"United States"},
{"id":11,"first_name":"Cathyleen","last_name":"Conklin","job":"VP Quality Control","salary":"$5.28","location":"Brazil"},
{"id":12,"first_name":"Stephani","last_name":"Trase","job":"Editor","salary":"$8.49","location":"Indonesia"},
{"id":13,"first_name":"Enoch","last_name":"Buddles","job":"Pharmacist","salary":"$3.63","location":"Peru"},
{"id":14,"first_name":"Jennilee","last_name":"Watsham","job":"Programmer Analyst I","salary":"$9.24","location":"Indonesia"},
{"id":15,"first_name":"Tadeas","last_name":"Caslake","job":"Research Nurse","salary":"$4.18","location":"Macedonia"},
{"id":16,"first_name":"Joell","last_name":"Snasdell","job":"Librarian","salary":"$6.62","location":"Croatia"},
{"id":17,"first_name":"Ariana","last_name":"McGillacoell","job":"Chief Design Engineer","salary":"$6.56","location":"China"},
{"id":18,"first_name":"Doro","last_name":"Perrinchief","job":"VP Sales","salary":"$0.89","location":"Zambia"},
{"id":19,"first_name":"Con","last_name":"Basso","job":"Automation Specialist I","salary":"$4.18","location":"China"},
{"id":20,"first_name":"Trixy","last_name":"Ayliffe","job":"Compensation Analyst","salary":"$0.49","location":"South Africa"},
{"id":21,"first_name":"Luelle","last_name":"Puffett","job":"Executive Secretary","salary":"$8.25","location":"China"},
{"id":22,"first_name":"Sylvester","last_name":"Novic","job":"Office Assistant I","salary":"$1.65","location":"South Korea"},
{"id":23,"first_name":"Harmon","last_name":"Pilch","job":"Structural Analysis Engineer","salary":"$0.48","location":"China"},
{"id":24,"first_name":"Nollie","last_name":"Cannavan","job":"Database Administrator I","salary":"$4.21","location":"Poland"},
{"id":25,"first_name":"Elena","last_name":"Asquez","job":"Software Test Engineer III","salary":"$9.42","location":"Azerbaijan"},
{"id":26,"first_name":"Scotty","last_name":"Fearnside","job":"Assistant Manager","salary":"$2.80","location":"Russia"},
{"id":27,"first_name":"Xaviera","last_name":"Wickrath","job":"Administrative Assistant I","salary":"$6.62","location":"Thailand"},
{"id":28,"first_name":"Basile","last_name":"Luna","job":"Data Coordiator","salary":"$2.61","location":"Chile"},
{"id":29,"first_name":"Fayth","last_name":"Lindop","job":"Health Coach II","salary":"$5.14","location":"Indonesia"},
{"id":30,"first_name":"Curtice","last_name":"Mea","job":"Senior Financial Analyst","salary":"$5.00","location":"Colombia"}]
/*
1. Find allEmp Web Developers
2. Convert allEmp the salary values into proper numbers instead of strings 
3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)
4. Find the sum of allEmp salaries 
5. Find the sum of allEmp salaries based on country using only HOF method
6. Find the average salary of based on country using only HOF method

NOTE: Do not change the name of this file
*/  
// Demo Data
// [
//     {
//       id: 1,
//       first_name: 'Gregg',
//       last_name: 'Lacey',
//       job: 'Web Developer III',
//       salary: '$3.62',
//       location: 'Malta'
//     },
//     {
//       id: 4,
//       first_name: 'Sherwood',
//       last_name: 'Adriano',
//       job: 'Web Developer II',
//       salary: '$6.46',
//       location: 'Brazil'
//     }
//   ]
//Find allEmp Web Developers
function allWebDevelopers(data){
    
    let profession="Web Developer";
    let allWebDev=[];
    data.filter((item)=>{
        if(item.job.includes(profession)){
            allWebDev.push(item);
        }
    });
    //console.log(allWebDev)
    return allWebDev;
}
// allWebDevelopers(array);


//Question 2
function salaryInNumber(data){
    let resultData = [];

    data.filter((item)=>{
        let pay = stringToNumber(item.salary);
        item["salary"]=pay;
        resultData.push(item);
    });
    //console.log(resultData);
    return resultData;
}
function stringToNumber (string) {
    if(typeof(string)=='string') {
      let result = "";
      let StrToArray=string.split("");
      StrToArray.filter((char)=>{
        if(char!='$'){
            result+=char;
        }
      });
      if(isNaN(result)){
        return 0
      }
      //console.log(result);
      return parseFloat(result);
    }
    return 0;
}
//salaryInNumber(array);
    


//Questioin 3
function salaryFactorOf(data,factor){
    let resultData=[];
    data.filter((item)=>{
        let pay = stringToNumber(item.salary);
        pay=pay*factor;
        item["corrected_salary"]=pay;
        resultData.push(item);
    });
    //console.log(resultData);
    return resultData;
}
//salaryFactorOf(array,10000);



//Question 4
function sumOfAllSalary(data){
    let grossAmount = 0;
    
    data.filter((item)=>{
        let pay = stringToNumber(item.salary);
        grossAmount+=pay;
    });
    //console.log(grossAmount);
    return grossAmount;
}
// sumOfAllSalary(array);



//Question 5
function sumOfAllSalaryBasedOnCountry(data){
    let temp = new Set();
    data.map((item)=>{
        temp.add(item.location);
    });
    
    //Converting temp set to iterable Array
    let allCountry = Array.from(temp);

    let result = [];
    allCountry.filter((country)=>{
        const tempData = data.filter((item)=>item.location==country);
        let initialValue = 0;
        let sumOfSalary = tempData.reduce(
            (accumulator, item) => accumulator + stringToNumber(item.salary),initialValue);
        result[country]=sumOfSalary;
    });
    //console.log(result);
    return result;
}
// sumOfAllSalaryBasedOnCountry(array);



//question 6
//Find the average salary of based on country using only HOF method

function avrageSalary(data){
    let allCountrySet = new Set();

    let allEmp=[];
    data.map((item)=>{
        //adding to allEmp 
        if (allEmp.hasOwnProperty(item.location)){
            allEmp[item.location]=allEmp[item.location]+1;
        }else{
            allEmp[item.location]=1;   
        }

        //adding name of country to the set
        allCountrySet.add(item.location)
    });

    //Converting temp set to iterable Array
    let allCountry=Array.from(allCountrySet);



    let avgSalary = [];
    //Addition od all the salary per country and stored it in an veriable result as key=countryName an Value=AmountOfSalary
    let result = {};
    allCountry.filter((country)=>{

        //Addition od all the salary per country and stored it in an veriable result as key=countryName an Value=AmountOfSalary
        const tempData = data.filter((item)=>item.location==country);
        let initialValue = 0;
        let sumOfSalary = tempData.reduce(
            (accumulator, item) => accumulator + stringToNumber(item.salary),
            initialValue
        );
        result[country]=sumOfSalary;


        //In Ordet to divide salary by Number of Employee in the perticular country
        if(allEmp.hasOwnProperty(country)){
            avgSalary[country]=result[country]/allEmp[country];
        }
    });
    
    return avgSalary;
}
//avrageSalary(array);