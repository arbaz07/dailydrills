const fs = require("fs");
const path = require("path");
/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/




function fileHandler(){
fs.readFile(path.join(__dirname,"./data.json"),"utf-8",(err,data)=>{
    if(err){
        console.error(err)
    }else{
        data = JSON.parse(data);
    // console.log(data["employees"]);
        let id = [2,13,23];
        let dataForIds = id.reduce((acc,currentId)=>{
            let ans = data["employees"].reduce((acc,element)=>{
                if(element["id"]==currentId){
                    acc[currentId]=element;
                }
                return acc;
            },{});
            acc[currentId]=ans;
            return acc;
        },{});
        // 1 Retrieve data for ids : [2, 13, 23]
        fs.writeFile("./result1.json",JSON.stringify(dataForIds),(err) =>{
        if(err){
            console.error(err);
        }else{
            console.log("done.");
            fs.readFile(path.join(__dirname,"./data.json"),"utf-8",(err,data)=>{
                if(err){
                    console.error(err)
                }else{
                    let companiesName = [ "Scooby Doo", "Powerpuff Brigade", "X-Men"]
                    data = JSON.parse(data);
                    let dataForCompany = companiesName.reduce((acc,currCompany)=>{
                        let ans = data["employees"].reduce((acc,element)=>{
                            if(element["company"]==currCompany){
                                acc.push(element);
                            }
                            return acc;
                        },[]);
                        acc[currCompany]=ans;
                        return acc;
                    },{});
                    // 2. Group data based on companies.
                    fs.writeFile("./result2.json",JSON.stringify(dataForCompany),(err) =>{
                        if(err){
                            console.error(err);
                        }else{
                            fs.readFile(path.join(__dirname,"./data.json"),"utf-8",(err,data)=>{
                                if(err){
                                    console.error(err)
                                }else{
                                    // 3. Get all data for company Powerpuff Brigade
                                    data = JSON.parse(data);
                                    let ans = data["employees"].reduce((acc,element)=>{
                                        if(element["company"]=="Powerpuff Brigade"){
                                            acc.push(element);
                                        }
                                        return acc;
                                    },[]);
                                    let result3 = {};
                                    result3["company"]=ans;
                                    
                                    fs.writeFile("./result3.json",JSON.stringify(result3),(err) =>{
                                        if(err){
                                            console.error(err);
                                        }else{
                                            fs.readFile(path.join(__dirname,"./data.json"),"utf-8",(err,data)=>{
                                                if(err){
                                                    console.error(err)
                                                }else{
                                                    data = JSON.parse(data);
                                                    let ans = data["employees"].reduce((acc,element)=>{
                                                        if(element["id"]!==2){
                                                            acc.push(element);
                                                        }
                                                        return acc;
                                                    },[]);
                                                    let result4 = {};
                                                    result4["employees"]=ans;
                                                    // 4. Remove entry with id 2.
                                                    fs.writeFile("./result4.json",JSON.stringify(result4),(err) =>{
                                                        if(err){
                                                            console.error(err);
                                                        }else{
                                                            fs.readFile(path.join(__dirname,"./data.json"),"utf-8",(err,data)=>{
                                                                if(err){
                                                                    console.error(err)
                                                                }else{
                                                                    // 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
                                                                    data = JSON.parse(data);
                                                                    let sortBasedOnCompany = data["employees"].sort((item1,item2)=>{
                                                                        return item1["company"].localeCompare(item2["company"])
                                                                    }) 
                                                                    let sortedData = sortBasedOnCompany.sort((item1,item2)=>{
                                                                        if(item1["company"]===item2["company"]){
                                                                            return item1["id"]-item2["id"];
                                                                        }
                                                                    })
                                                                    fs.writeFile("./result5.json",JSON.stringify(sortedData),(err) =>{
                                                                        if(err){
                                                                            console.error(err);
                                                                        }else{
                                                                            fs.readFile(path.join(__dirname,"./data.json"),"utf-8",(err,data)=>{
                                                                                if(err){
                                                                                    console.error(err)
                                                                                }else{
                                                                                   // 6. Swap position of companies with id 93 and id 92.
                                                                                    data = JSON.parse(data);
                                                                                    let temp1 = {};
                                                                                    let temp2 = {};
                                                                                    for(let index = 0; index< data["employees"].length; index++){
                                                                                        if(data["employees"][index]["id"]==93){
                                                                                            temp1 = data["employees"][index];
                                                                                        }
                                                                                        if(data["employees"][index]["id"]==92){
                                                                                            temp2=data["employees"][index];
                                                                                            
                                                                                        }
                                                                                    }
                                                                                    
                                                                                    for(let index = 0; index< data["employees"].length; index++){
                                                                                        if(data["employees"][index]["id"]==93){
                                                                                            data["employees"][index]=temp2;  
                                                                                            continue;
                                                                                        }
                                                                                        if(data["employees"][index]["id"]==92){
                                                                                            data["employees"][index]=temp1; 
                                                                                        }
                                                                                    }
                                                                                    fs.writeFile("./result6.json",JSON.stringify(data),(err) =>{
                                                                                        if(err){
                                                                                            console.error(err); 
                                                                                        }else{
                                                                                            fs.readFile(path.join(__dirname,"./data.json"),"utf-8",(err,data)=>{
                                                                                                if (err){
                                                                                                    console.error(err);
                                                                                                }else{
                                                                                                    
                                                                                                    data = JSON.parse(data);
                                                                                                    let result7 = data["employees"].map((element)=>{
                                                                                                        if(isEven(element["id"])){
                                                                                                            // console.log(element);
                                                                                                            element["dop"] = new Date()
                                                                                                        }
                                                                                                        return element  
                                                                                                    })
                                                                                                    fs.writeFile("./result7.json",JSON.stringify(data),(err) =>{
                                                                                                        if(err){
                                                                                                            console.error(err); 
                                                                                                        }else{
                                                                                                            console.log("Process Completed");
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });    
                                                                        }
                                                                    });
                                                                }
                                                            });    
                                                        }
                                                    });
                                                }
                                            });    
                                        }
                                    });
                                }
                            });    
                        }
                    });
                }
            });    
        }
    });
    }
})
}
fileHandler();

function isEven(num){
    if(num%2==0){
        return true
    }else{
        return false;

    }
}

// const emp = {
//     "employees": [
//         {
//             "id": 23,
//             "name": "Daphny",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 73,
//             "name": "Buttercup",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 93,
//             "name": "Blossom",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 13,
//             "name": "Fred",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 89,
//             "name": "Welma",
//             "company": "Scooby Doo"
//         },
//         {
//             "id": 92,
//             "name": "Charles Xavier",
//             "company": "X-Men"
//         },
//         {
//             "id": 94,
//             "name": "Bubbles",
//             "company": "Powerpuff Brigade"
//         },
//         {
//             "id": 2,
//             "name": "Xyclops",
//             "company": "X-Men"
//         }
//     ]
// }


