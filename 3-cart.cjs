const products = [
    {
        shampoo: {
            price: "$50",
            quantity: 4,
        },
        "Hair-oil": {
            price: "$40",
            quantity: 2,
            sealed: true,
        },
        comb: {
            price: "$12",
            quantity: 1,
        },
        utensils: [
            {
                spoons: { quantity: 2, price: "$8" },
            },
            {
                glasses: { quantity: 1, price: "$70", type: "fragile" },
            },
            {
                cooker: { quantity: 4, price: "$900" },
            },
        ],
        watch: {
            price: "$800",
            quantity: 1,
            type: "fragile",
        },
    },
];

/*


NOTE: Do not change the name of this file

*/
// Q1. Find all the items with price more than $65.
function priceMoreThan(data,minPrice){
    // console.log("asd")
    let ans = data.reduce((accc,item)=>{
        let subAns = Object.entries(item).reduce((acc,subItem)=>{
            if(subItem[1] instanceof Array){
                let subSubItem = subItem[1].reduce((subacc,item2)=>{
                    let item3=Object.entries(item2)
                    if(stringToNum(item3[0][1].price)>minPrice){
                        subacc[item3[0][0]]=item3[0][1];
                    }
                    return subacc;
                },{})
                acc[subItem[0]]=subSubItem;
                return acc;
            }
            if(stringToNum(subItem[1].price)>minPrice){
                acc[subItem[0]]=subItem[1];
            }
            return acc
        },{})
        accc=subAns;
        return accc;
    },{})
    return ans;
}
// priceMoreThan(products,65);







// Q2. Find all the items where quantity ordered is more than 1.
function quantitytyMoreThan(data,unit){
    // console.log("asd")
    let ans = data.reduce((accumulator,item)=>{
        let subAns = Object.entries(item).reduce((acc,subItem)=>{
            if(subItem[1] instanceof Array){
                let subSubItem = subItem[1].reduce((subacc,item2)=>{
                    let item3=Object.entries(item2)
                    if(item3[0][1].quantity>unit){
                        subacc[item3[0][0]]=item3[0][1];
                    }
                    return subacc;
                },{})
                acc[subItem[0]]=subSubItem;
                return acc;
            }
            if(subItem[1].quantity>unit){
                acc[subItem[0]]=subItem[1];
            }
            return acc
        },{})
        accumulator=subAns;
        return accumulator;
    },{})
    return ans;
}
// quantitytyMoreThan(products,1);



// Q.3 Get all items which are mentioned as fragile.
function itemMentionedAsFragile(data){
    // console.log("asd")
    let ans = data.reduce((accumulator,item)=>{

        let subAns = Object.entries(item).reduce((acc,subItem)=>{
            if(subItem[1] instanceof Array){
                let subSubItem = subItem[1].reduce((subacc,item2)=>{
                    let item3=Object.entries(item2)
                    if(item3[0][1].type!=undefined){
                        subacc[item3[0][0]]=item3[0][1];
                    }
                    return subacc;
                },{})
                acc[subItem[0]]=subSubItem;
                return acc;
            }
            if(subItem[1].type!=undefined){
                acc[subItem[0]]=subItem[1];
            }
            return acc
        },{})
        accumulator=subAns;
        return accumulator;
    },{})
    return ans;
}
// itemMentionedAsFragile(products,1);



// Q.4 Find the least and the most expensive item for a single quantity.
function leastAndMostExpensive(data){

    let utensile = data[0].utensils;
    delete(data[0].utensils);

    console.log(utensile);
    let sortedUtensile = sortPrice(utensile);
    let maxForsortedUtensile = sortedUtensile[0];
    let minForsortedUtensile = sortedUtensile[sortedUtensile.length-1];

    let sorteditems = sortPrice(data);
    let maxForSorteditems = sorteditems[0];
    let minForSorteditems = sorteditems[sorteditems.length-1];

    let ansObj = []
    if(stringToNum(maxForsortedUtensile[1].price)>stringToNum(maxForSorteditems[1].price)){
        ansObj.push(maxForsortedUtensile);
    }else{
        ansObj.push(maxForSorteditems);
    }


    if(stringToNum(minForSorteditems[1].price)>=stringToNum(minForsortedUtensile[1].price)){
        ansObj.push(minForSorteditems);
    }else{
        ansObj.push(minForsortedUtensile);
    }

    console.log( ansObj); 
    
}
leastAndMostExpensive(products);





// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)
function groupBaseOnRoomTemp(data){
        let utensilsDetails = data[0].utensils;
      
        let utensilProducts = utensilsDetails.map((data) =>{
        return Object.entries(data).flat();
        })
      
        let reducer = utensilProducts.reduce((acc,element) =>{
            acc[element[0]] = element[1];
            return acc;
        },[])
      
        let obj = Object.assign(data[0],reducer);
      
        delete obj.utensils;
      
        let result ={
          ['solid'] : {
            ["comb"] :obj.comb,
            ["watch"] :obj.watch,
            ["spoons"] : obj.spoons,
            ["glasses"] :obj.glasses,
            ["cooker"] :obj.cooker
          },
          ['liquid'] :{
            ["shampoo"]:obj.shampoo,
            ["Hair-oil"]:obj["Hair-oil"]
          },
          ['gas']:{}
        }
        return result;
      
   
}

// console.log(groupBaseOnRoomTemp(products));















function stringToNum(str){
    if(str!=undefined){
        return parseInt(str.replace("$",""));
    }
    return 0
}






function sortPrice(data){
    let ans = Object.entries(data)
        .sort(([,item1], [,item2]) =>{
            let p1 = stringToNum(item1.price)/Number(item1.quantity);
            console.log(item1.quantity)
            let p2 = stringToNum(item2.price)/Number(item1.quantity);
            return p2-p1;
        });
    return ans; 
}







function quantityequalTo(data,unit){
    // console.log("asd")
    let ans = data.reduce((accumulator,item)=>{
        let subAns = Object.entries(item).reduce((acc,subItem)=>{
            if(subItem[1] instanceof Array){
                let subSubItem = subItem[1].reduce((subacc,item2)=>{
                    let item3=Object.entries(item2)
                    if(item3[0][1].quantity=unit){
                        subacc[item3[0][0]]=item3[0][1];
                    }
                    return subacc;
                },{})
                acc[subItem[0]]=subSubItem;
                return acc;
            }
            if(subItem[1].quantity=unit){
                acc[subItem[0]]=subItem[1];
            }
            return acc
        },{})
        accumulator=subAns;
        return accumulator;
    },{})
    return ans;
}